# Page basique

## Ligne avec page coupée en 2, image gauche, texte droite

```html
<div class="container">
    <div class="row">
        <div class="col-12 col-md-6">
            <img src="https://s3.eu-central-1.amazonaws.com/brz-la-box-du-vin/StockSnap_RDUYGQAMBL.jpg" class="img-fluid" alt="">
        </div>
        <div class="col-12 col-md-6">
            <h3 class="mt-4">Votre IMC est de 27</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequatur, dignissimos esse eveniet fugit impedit itaque quam qui quia quibusdam, quisquam ratione. Assumenda aut consequuntur enim iure modi repellat veritatis?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequatur, dignissimos esse eveniet fugit impedit itaque quam qui quia quibusdam, quisquam ratione. Assumenda aut consequuntur enim iure modi repellat veritatis?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequatur, dignissimos esse eveniet fugit impedit itaque quam qui quia quibusdam, quisquam ratione. Assumenda aut consequuntur enim iure modi repellat veritatis?</p>
        </div>
    </div>
</div>
```

## Ligne avec page coupée en 2, image droite, texte gauche

```html
<div class="container">
    <div class="row">
      <div class="col-12 col-md-6">
            <h3 class="mt-4">Votre IMC est de 27</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequatur, dignissimos esse eveniet fugit impedit itaque quam qui quia quibusdam, quisquam ratione. Assumenda aut consequuntur enim iure modi repellat veritatis?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequatur, dignissimos esse eveniet fugit impedit itaque quam qui quia quibusdam, quisquam ratione. Assumenda aut consequuntur enim iure modi repellat veritatis?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequatur, dignissimos esse eveniet fugit impedit itaque quam qui quia quibusdam, quisquam ratione. Assumenda aut consequuntur enim iure modi repellat veritatis?</p>
       </div>
       <div class="col-12 col-md-6">
            <img src="https://s3.eu-central-1.amazonaws.com/brz-la-box-du-vin/StockSnap_RDUYGQAMBL.jpg" class="img-fluid" alt="">
        </div>
    </div>
</div>
```

### Ligne avec 3 étapes illustrées image + description :

```html
<div class="container">
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2"><h2 class="text-center my-4">Trois raisons d'utiliser&nbsp; <strong>BioBox</strong>
        </h2>
            <div class="separator"></div>
            <p class="my-4 text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam voluptas
                facere vero ex tempora saepe perspiciatis ducimus sequi animi.</p></div>
        <div class="col-12 col-md-4">
            <div class="py-3 feature-box text-center"><img alt=""
                                                           src="https://s3.eu-central-1.amazonaws.com/brz-la-box-du-vin/step-1.png">
                <h5 class="my-3">Découvrez</h5>
                <p class="text-secondary">Voluptatem ad provident non repudiandae beatae cupiditate amet reiciendis
                    lorem ipsum dolor sit amet, consectetur.</p></div>
        </div>
        <div class="col-12 col-md-4">
            <div class="py-3 feature-box text-center"><img alt=""
                                                           src="https://s3.eu-central-1.amazonaws.com/brz-la-box-du-vin/step-2.png">
                <h5 class="my-3">Apprenez</h5>
                <p class="text-secondary">Iure sequi unde hic. Sapiente quaerat sequi inventore veritatis cumque lorem
                    ipsum dolor sit amet, consectetur.</p></div>
        </div>
        <div class="col-12 col-md-4">
            <div class="py-3 feature-box text-center"><img alt=""
                                                           src="https://s3.eu-central-1.amazonaws.com/brz-la-box-du-vin/step-3.png">
                <h5 class="my-3">Achetez</h5>
                <p class="text-secondary">Inventore dolores aut laboriosam cum consequuntur delectus sequi lorem ipsum
                    dolor sit amet, consectetur.</p></div>
        </div>
    </div>
</div>
```


# Page avancées :


## Fond alternatif avec couleur dominante : 

```html
<div class="bg-primary py-3">
  
</div>
```

## Fond alternatif gris : 
```html
<div class="bg-light py-3">
  
</div>
```


# Produit

## Tabulation
```html
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" href="#whatin" role="tab" data-toggle="tab">Ingrédients</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#nutval" role="tab" data-toggle="tab">Valeur nutritionnelle</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content p-4">
    <div role="tabpanel" class="tab-pane fade in active show" id="whatin">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A blanditiis corporis debitis delectus dicta dolorum ex iusto, necessitatibus quaerat quam sequi unde vel voluptas! Cum dolore error in ipsa repudiandae!
    </div>
    <div role="tabpanel" class="tab-pane fade" id="nutval">
        <img alt="" src="https://s3.eu-central-1.amazonaws.com/brz-mon-panier-minceur/conseils.jpg" class="img-fluid">
    </div>
</div>
```


# Classes utiles :

texte centré : text-center  
ajouter une marge au dessus et en dessous : : my-4  
ajouter du remplissage au dessus et en dessous : : py-4  
couleur de texte avec la couleur dominante : text-primary  
texte gris : text-secondary  




